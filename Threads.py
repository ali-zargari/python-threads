import threading
import time

def thread1():
    print("\nThread1 started")

    for i in range(0,11):
        var = "thread1 counter: " + str(i) + " "
        print(var)
        time.sleep(1)

    print("Thread1 finished ")

def thread2():
    print("\nThread2 started")

    for i in range(10,-1,-1):
        var2 = "thread2 counter: " + str(i) + " "
        print(var2)
        time.sleep(1)

    print("Thread2 finished ")


if __name__ == '__main__':

    t1 = threading.Thread(target=thread1)
    t2 = threading.Thread(target=thread2)

    t1.start()
    t2.start()

    t1.join()
    t2.join()

    print("Main finished")
